import string
import re
import random
from enum import Enum, IntEnum, unique
from threading import Thread
from pathlib import Path
from queue import Queue
from pandas.core.frame import DataFrame
from pandas import read_csv, concat
from numpy import bool_
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QTableWidgetItem
from form import Ui_MainWindow

class App(QtWidgets.QMainWindow, Ui_MainWindow):
    @unique
    class errorCodes(Enum):
        SUCCESS = (0, "Success")
        EMPTY = (1, "Emtpy fields")
        WRONG = (2, "Wrong data")

    @unique
    class appPages(IntEnum):
        ACC = 0
        ADMIN = 1
        PSW = 2
        USER = 3

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.aboutAction.triggered.connect(lambda: self.create_message("О программе",
        "<html><head/><body><p><span style=\" font-size:14pt;\">Автор: Крючков Павел ИДБ-18-08</span></p><p><span style=\" font-size:14pt;\">Вариант 8.</span></p></body></html>"))
        self.loginEdit.returnPressed.connect(self.entry_attempt)
        self.passwordEdit.returnPressed.connect(self.entry_attempt)
        self.accButton.released.connect(self.entry_attempt)
        self.adminextButton.released.connect(lambda: self.to_page(self.appPages.ACC))
        self.userextButton.released.connect(lambda: self.to_page(self.appPages.ACC))
        self.extButton.released.connect(lambda: self.to_page(self.appPages.ACC))
        self.changePswButton.released.connect(lambda: self.to_page(self.appPages.PSW))
        self.returnButton.released.connect(lambda: self.to_page(self.appPages.USER))
        self.saveButton.released.connect(self.save_table)
        self.newuserButton.released.connect(self.add_user)
        self.pswButton.released.connect(self.change_password)
        self.adminChButton.released.connect(lambda: self.to_page(self.appPages.PSW))

        self.threads = {}
        self.df: DataFrame = None

        if Path('users.csv').is_file():
            self.df = read_csv('users.csv', index_col='Имя пользователя')
        else:
            self.df = DataFrame({
                'Имя пользователя': ['ADMIN'],
                'Пароль': ['ADMIN'],
                'Блокировка': [False],
                'Парольное ограничение': [False],
                'Только создан': [False]
            }).set_index('Имя пользователя')
            self.df.to_csv('users.csv')
        
        self.users_df: DataFrame = self.df.drop(columns=['Пароль', 'Только создан']).filter(regex='^(?!ADMIN)', axis=0).reset_index(level=0)

        self.q = Queue()
        ui_thread = Thread(target=self.__populate_table, args=(self.q, ))
        self.threads['ui'] = ui_thread
        ui_thread.start()

        self.cur_user: str = None

        self.attempts = 3

    def closeEvent(self, event):
        print('CLOSING')
        for thread in self.threads:
            self.threads[thread].join()

    def create_message(self, title: str, text: str):
        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle(title)
        msg.setText(text)
        msg.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        return msg.exec_()

    def to_page(self, appPage: appPages, login=None) -> None:
        if login:
            self.cur_user = login
        if self.cur_user == 'ADMIN':
            if appPage == self.appPages.USER:
                appPage = self.appPages.ADMIN
        else:
            if appPage==self.appPages.PSW:
                if self.df.loc[self.cur_user]['Только создан']:
                    self.create_message('Первый вход', 'Вы вошли впервые. Пожалуйста, смените пароль.')
                elif self.df.loc[self.cur_user]['Парольное ограничение']:
                    self.create_message('Парольное ограничение', 'Пожалуйста, смените пароль. Он должен содержать латинские буквы и символы кириллицы.')
        self.oldestpswEdit.setText('')
        self.oldpswEdit.setText('')
        self.nwepswEdit.setText('')
        self.stackedWidget.setCurrentIndex(appPage) 

    def entry_attempt(self) -> errorCodes:
        login: str = self.loginEdit.text()
        password: str = self.passwordEdit.text()
        if login == ''  or password == '':
            return self.errorCodes.EMPTY
        elif login in self.df.index.values and self.df.loc[login]['Блокировка']:
            self.create_message('Ошибка входа', 'Вы заблокированы.')
            return self.errorCodes.WRONG
        elif login in self.df.index.values and password == self.df.loc[login]['Пароль']:
            self.cur_user = login
            if login == 'ADMIN':
                self.returnButton.setVisible(True)
                self.to_page(self.appPages.ADMIN, login=login)
            else:
                if self.df.loc[self.cur_user]['Парольное ограничение'] or self.df.loc[self.cur_user]['Только создан']:
                    self.returnButton.setVisible(False)
                    self.to_page(self.appPages.PSW, login=login)
                else:
                    self.returnButton.setVisible(True)
                    self.to_page(self.appPages.USER, login=login)
            
            self.loginEdit.setText('')
            self.passwordEdit.setText('')
            return self.errorCodes.SUCCESS
        elif login in self.df.index.values:
            self.attempts -= 1
            if self.attempts == 0:
                self.close()
            else:
                self.create_message('Ошибка входа', f'Неверно введён логин или пароль.\nОсталось попыток: {self.attempts}')
                return self.errorCodes.WRONG
        else:
           self.create_message('Ошибка входа', f'Неверно введён логин или пароль.')
           return self.errorCodes.WRONG

    def __populate_table(self, q: Queue):
        nRows, nColumns = self.users_df.shape
        self.usersTableView.setColumnCount(nColumns)
        self.usersTableView.setRowCount(nRows)
        self.usersTableView.setHorizontalHeaderLabels(self.users_df.columns.values.tolist())
        for i in range(self.usersTableView.rowCount()):
            for j in range(self.usersTableView.columnCount()):
                if isinstance(self.users_df.iat[i, j], bool_):
                    item = QtWidgets.QTableWidgetItem()
                    item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
                    item.setCheckState(QtCore.Qt.Checked if bool(self.users_df.iat[i, j]) else QtCore.Qt.Unchecked)
                    self.usersTableView.setItem(i, j, item)
                else:
                    self.usersTableView.setItem(i, j, QTableWidgetItem(str(self.users_df.iat[i, j])))

        self.usersTableView.cellChanged[int, int].connect(self.updateDF)
        
        q.join()

    def add_user(self):
        new_login: str = self.newuserEdit.text()
        if new_login == '':
            self.create_message('Ошибка', 'Введите логин нового пользователя.')
        elif new_login in self.df.index:
            self.create_message('Ошибка', 'Пользователь с таким логином уже существует.')
        else:
            new_pass = self.__generate_password()
            new_df = DataFrame({
                'Имя пользователя': [new_login],
                'Пароль': [new_pass],
                'Блокировка': [False],
                'Парольное ограничение': [False],
                'Только создан': [True]
            }).set_index('Имя пользователя')
            self.df = concat([self.df, new_df])
            self.users_df = self.df.drop(columns=['Пароль', 'Только создан']).filter(regex='^(?!ADMIN)', axis=0).reset_index(level=0)
            self.__populate_table(self.q)
            self.create_message('Новый пользователь', f'Пароль нового пользователя {new_login}: {new_pass}')
            self.df.to_csv("users.csv")

    def __generate_password(self, size=6, chars=string.ascii_letters, check=False):
        glagolitsa = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
        glagolitsa += glagolitsa.lower()
        if check:
            chars += glagolitsa
        return ''.join(random.choice(chars) for _ in range(size))

    def has_cyrillic(self, text):
        return bool(re.search('[а-яА-Я]', text))
    
    def has_latin(self, text):
        return bool(re.search('[a-zA-Z]', text))

    def change_password(self):
        old_psw: str = self.oldestpswEdit.text()
        new_pass: str = self.oldpswEdit.text()
        confirm_pass: str = self.nwepswEdit.text()
        if old_psw == '':
            return self.create_message('Ошибка', 'Введите старый пароль.')
        elif new_pass == '':
            return self.create_message('Ошибка', 'Введите новый пароль.')
        elif confirm_pass == '':
            return self.create_message('Ошибка', 'Введите подтверждение пароля.')
        elif old_psw == self.df.loc[self.cur_user]['Пароль']:
            if new_pass == confirm_pass:
                if old_psw == new_pass:
                     return self.create_message('Ошибка', 'Старый и новый пароли не должны совпадать')
                elif self.df.loc[self.cur_user]['Парольное ограничение']:
                    if not (self.has_cyrillic(new_pass) and self.has_latin(new_pass)):
                        return self.create_message('Ошибка', 'Ваш пароль должен содержать латинские буквы и символы кириллицы.')
                
                self.df.at[self.cur_user, 'Пароль'] = new_pass
                if self.df.loc[self.cur_user]['Только создан']:
                    self.df.at[self.cur_user, 'Только создан'] = False
                self.df.to_csv("users.csv")

                self.create_message('Новый пароль', 'Ваш пароль успешно изменён.')
                self.oldestpswEdit.setText('')
                self.oldpswEdit.setText('')
                self.nwepswEdit.setText('')
                self.to_page(self.appPages.USER)
            else:
                self.create_message('Ошибка', 'Ваш пароль и его подтверждение не совпадают.')
        else:
            return self.create_message('Ошибка', 'Неверно введён пароль.')

    def save_table(self):
        self.df.update(self.users_df.set_index('Имя пользователя'))
        self.df.to_csv("users.csv")
        self.create_message('Сохранение', 'Данные успешно сохранены.')

    def updateDF(self, row, column):
        check_state: bool = False if self.usersTableView.item(row, column).checkState() == 0 else True
        self.users_df.iat[row, column] = check_state
